package facci.gabrielaguaman.volleye;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.MyViewHolder> {

    ArrayList<Modelo> modelos;

    public Adaptador(ArrayList<Modelo> modelos) {
        this.modelos = modelos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler, viewGroup,
                false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Modelo modelo = modelos.get(i);
        myViewHolder.campo_u.setText(modelo.getCampo_u());
        myViewHolder.campo_d.setText(modelo.getCampo_d());
        myViewHolder.campo_t.setText(modelo.getCampo_t());
        myViewHolder.campo_c.setText(modelo.getCampo_c());
        
        //myViewHolder.imagen.setImageBitmap(modelo.getImagen());

    }

    @Override
    public int getItemCount() {
       return modelos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView campo_u, campo_d, campo_t, campo_c;
        private ImageView imagen;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            campo_u = (TextView)itemView.findViewById(R.id.campo_u);
            campo_d = (TextView)itemView.findViewById(R.id.campo_d);
            campo_t = (TextView)itemView.findViewById(R.id.campo_t);
            campo_c = (TextView)itemView.findViewById(R.id.campo_c);
            imagen = (ImageView)itemView.findViewById(R.id.imagen);
        }
    }
}
