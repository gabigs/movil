package facci.gabrielaguaman.volleye;

import android.widget.ImageView;

public class Modelo {
    private String id;
    private String campo_u;
    private String campo_d;
    private String campo_t;
    private String campo_c;
    private ImageView imagen;

    public Modelo(String id, String campo_u, String campo_d, String campo_t, String campo_c, ImageView imagen) {
        this.id = id;
        this.campo_u = campo_u;
        this.campo_d = campo_d;
        this.campo_t = campo_t;
        this.campo_c = campo_c;
        this.imagen = imagen;
    }

    public Modelo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampo_u() {
        return campo_u;
    }

    public void setCampo_u(String campo_u) {
        this.campo_u = campo_u;
    }

    public String getCampo_d() {
        return campo_d;
    }

    public void setCampo_d(String campo_d) {
        this.campo_d = campo_d;
    }

    public String getCampo_t() {
        return campo_t;
    }

    public void setCampo_t(String campo_t) {
        this.campo_t = campo_t;
    }

    public String getCampo_c() {
        return campo_c;
    }

    public void setCampo_c(String campo_c) {
        this.campo_c = campo_c;
    }

    public ImageView getImagen() {
        return imagen;
    }

    public void setImagen(ImageView imagen) {
        this.imagen = imagen;
    }
}
