package facci.gabrielaguaman.volleye;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String URLE = "http://10.32.28.214:3005/jugadores/equipo/emelec";
    private ArrayList<Modelo> modelos;
    private Adaptador adaptador;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        modelos = new ArrayList<>();
        adaptador = new Adaptador(modelos);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_e);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        equipo();
    }

    private void equipo() {
        StringRequest stringRequest2 = new StringRequest(Request.Method.GET, URLE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i =0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Modelo modelo = new Modelo();
                        modelo.setCampo_u(jsonObject.getString("nombre"));
                        modelo.setCampo_d(jsonObject.getString("apellido"));
                        modelo.setCampo_t(jsonObject.getString("apodo"));
                        modelo.setCampo_c(jsonObject.getString("equipo"));
                        Picasso.get().load(jsonObject.getString("imagen"));
                        modelos.add(modelo);
                    }
                    recyclerView.setAdapter(adaptador);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", String.valueOf(error));
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest2);

    }
}
